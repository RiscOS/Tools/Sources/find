# Makefile for find

COMPONENT ?= find
INSTAPP = ${INSTDIR}${SEP}!Find

LIBS = ${CLXLIB}
CDEFINES = -DDDE
INSTAPP_FILES = !Boot !Help !Run Desc Messages Templates \
                !Sprites:Themes !Sprites11:Themes !Sprites22:Themes \
                Morris4.!Sprites:Themes.Morris4 Morris4.!Sprites22:Themes.Morris4
INSTAPP_VERSION = Desc

include CApp

# Dynamic dependencies:
